# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Foris Diagnostics
# plugin package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: Foris Diagnostics plugin\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-25 15:53+0200\n"
"PO-Revision-Date: 2018-11-02 09:24+0000\n"
"Last-Translator: Zoli <boritek@gmail.com>\n"
"Language-Team: Hungarian <https://hosted.weblate.org/projects/turris/"
"foris-diagnostics-plugin/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.3-dev\n"
"Generated-By: Babel 2.6.0\n"

msgid "Diagnostics"
msgstr "Diagnosztika"

msgid "Modules"
msgstr "Modulok"

msgid "Missing"
msgstr "Hiányzik"

msgid "Preparing"
msgstr "Előkészítés"

msgid "Ready"
msgstr "Kész"

msgid "Unknown"
msgstr "Ismeretlen"

#, python-format
msgid "Unable to get diagnostic \"%s\"."
msgstr "Nem lehet lekérni a(z) %s diagnosztikát."

#, python-format
msgid "Diagnostic \"%s\" removed."
msgstr "%s diagnosztika eltávolítva."

#, python-format
msgid "Unable to remove diagnostic \"%s\"."
msgstr "Nem lehet a(z) %s diagnosztikát eltávolítani."

#, python-format
msgid "Diagnostic \"%s\" is being prepared."
msgstr "\"%s\" diagnosztika készül."

msgid "Failed to generate diagnostic."
msgstr "Nem sikerült létrehozni a diagnosztikát."

msgid ""
"This page is dedicated to create diagnostics which can be useful to us to"
" debug some problems related to the router's functionality. "
msgstr ""
"Ez az oldal arra van szentelve, hogy diagnosztikákat készítsen, amely "
"hasznos lehet a számunkra, hogy az útválasztó működőképességével kapcsolatos "
"bizonyos hibákat megtaláljuk. "

msgid "Hint"
msgstr "Tipp"

msgid "Generate"
msgstr "Létrehozás"

msgid "List"
msgstr "Lista"

msgid ""
"Some of the diagnostics might contain a sensitive data so make sure to "
"remove it before sharing."
msgstr ""
"Néhány diagnosztika érzékeny adatokat tartalmazhat, ezért győződjön meg "
"arról, hogy megosztás előtt eltávolította őket."

msgid "ID"
msgstr "Azonosító"

msgid "status"
msgstr "állapot"

msgid "Download"
msgstr "Letöltés"

msgid "Remove"
msgstr "Eltávolítás"

msgid "Note that this list is not persistent and will be removed after reboot."
msgstr ""
"Figyeljünk arra, hogy ez a lista nem kerül megjegyzésre, újraindítás után"
" elveszik."
